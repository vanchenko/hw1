public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws IllegalArgumentException {
        if (n % 2 != 0) {
            throw new IllegalArgumentException("MyEvenNumber может использоваться только с четным числом.");
        }
        this.n = n;
    }

    public int getValue() {
        return n;
    }
}
