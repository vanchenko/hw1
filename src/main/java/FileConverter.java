import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileConverter {
    public static void convertToUppercase(String inputFilePath, String outputFilePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String uppercaseLine = line.toUpperCase();
                writer.write(uppercaseLine);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


//String inputFilePath = "./input.txt";
//String outputFilePath = "./output.txt";
//FileConverter.convertToUppercase(inputFilePath, outputFilePath);


