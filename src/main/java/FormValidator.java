import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {

    public static void checkName(String str) {
        if (str == null || str.length() < 2 || str.length() > 20 || !Character.isUpperCase(str.charAt(0))) {
            throw new IllegalArgumentException("Длина имени должна быть от 2 до 20 символов, первая буква заглавная.");
        }
    }

    public static void checkBirthday(String str) {
        LocalDate date = LocalDate.parse(str, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        LocalDate minDate = LocalDate.of(1900, 1, 1);
        LocalDate maxDate = LocalDate.now();
        if (date.isBefore(minDate) || date.isAfter(maxDate)) {
            throw new IllegalArgumentException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.");
        }
    }

    public static void checkGender(String str) {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Пол должен быть Male или Female.");
        }
    }

    public static void checkHeight(String str) {
        try {
            double height = Double.parseDouble(str);
            if (height <= 0) {
                throw new IllegalArgumentException("Высота должна быть положительным числом.");
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Высота должна быть числом.");
        }
    }
}
enum Gender {
    Male, Female
}