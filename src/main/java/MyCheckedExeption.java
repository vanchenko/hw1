public class MyCheckedExeption extends Exception {
    public MyCheckedExeption() {
        super();
    }

    public MyCheckedExeption(String message) {
        super(message);
    }

    public MyCheckedExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public MyCheckedExeption(Throwable cause) {
        super(cause);
    }
}