import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n = inputN();
        System.out.println("Успешный ввод!");

    }

    private static int inputN() {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n <= 0 || n >= 100){
            throw new IllegalArgumentException("Некорректный ввод");
        }
        return n;
    }
}